# SSG5 - Simple static site generator

This is my fork of the [ssg5](https://www.romanzolotarev.com/ssg.html).
I use it for my personal site [villeklar.com](https://villeklar.com/).

Main differences between this fork and the original one: 

- _render_articles_ function, inspired by this [video](https://www.youtube.com/watch?v=N_ttw2Dihn8)
    - also gets the date of the post and sorts them by date.
- the invocation of [lowdown](https://kristaps.bsd.lv/lowdown/) is slightly altered to enable mixing raw html in the markdown files.
- **ssg5watch** script uses [entr](https://github.com/eradman/entr/) to re-generate the output when the source files are changed.
